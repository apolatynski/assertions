package assertions

import java.util.*

class Strings {

    fun id() = "ABC12x"

    fun trim(input: String, maxLength: Int = DEFAULT_TRIM_LENGTH): String {
        val matches = WORD_REGEX.findAll(input)
        var lastBoundary = 0

        for (match in matches) {
            if (match.range.last < maxLength) {
                lastBoundary = match.range.last
            } else {
                if (lastBoundary == 0) {
                    lastBoundary = maxLength - 1
                }
                break
            }
        }

        return input.substring(0..lastBoundary).replaceFirst('\n', ' ')
    }

    fun generateUuid() = UUID.randomUUID().toString()

    companion object {
        const val DEFAULT_TRIM_LENGTH = 20
        val WORD_REGEX = Regex("(\\w+\\b)")
    }
}
